package Exception;


public class ContaInexistente extends Exception {

   public long numero;

    public ContaInexistente (long n) {
        super ("Conta Inexistente!");
        numero = n;
    }

    public long getNumero() {
            return numero;
    }
}
